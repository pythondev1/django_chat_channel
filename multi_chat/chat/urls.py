from django.urls import path
from .views import index, room, allmesg


urlpatterns = [
    path('', index, name='index'),
    path('room/<str:room_name>/', room, name='room'),
    path('allmesg/', allmesg, name='allmesg')
]